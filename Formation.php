<html>
<head>
	<title>Formation</title>
	<?php include 'include/head-1.php'?>
<body>
<header>
	<?php include 'include/header-1.php'?>
	<div class="second-title">
		<div class="second-title-inner">
			<img src="images/Formation_titre2.png" />
		</div>
	</div>
</header>

<main class="main-content">
		<div class="content-part">
			<div class="title-content">
				<div>
					<h2>Répartition des enseignements</h2>
				</div>
			</div>
			<div class="section-content">
				<div class="menu-content">
					<li>Semestre 1</li>
					<li>Semestre 2</li>
					<li>Commentaires</li>
				</div>
				<div class="container-content">
					<h3>Semestre 1</h3>
					<table>
						<tr>
							<th>Catégorie</th>
							<th>Discipline</th>
							<th>Volume horaire</th>
							<th>Coefficients</th>
						</tr>
						<tr class="title-UE">
							<td colspan="2">Bases de l'informatique</td>
							<td>256</td>
							<td>17</td>
						</tr>
						<tr>
							<td rowspan="6"></td>
							<td>Introduction aux systèmes informatiques</td>
							<td>54</td>
							<td>3.5</td>
						</tr>
						<tr>
							<td>Introduction à l'algorithme et à la programmation</td>
							<td>59</td>
							<td>3.5</td>
						</tr>
						<tr>
							<td>Structures de données et algorithmes fondamentaux</td>
							<td>41</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Introduction aux bases de données</td>
							<td>58</td>
							<td>3.5</td>
						</tr>
						<tr>
							<td>Conception de documents et d'interfaces numériques</td>
							<td>44</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Projet tutoré</td>
							<td>0 (Travail personnel)</td>
							<td>1.5</td>
						</tr>
						<tr class="title-UE">
							<td colspan="2">Bases de culture scientifique sociale et humaine</td>
							<td>208</td>
							<td>13</td>
						</tr>
						<tr>
							<td rowspan="6"></td>
							<td>Mathématiques discrètes</td>
							<td>42</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Algèbre linéaire</td>
							<td>30</td>
							<td>2</td>
						</tr>
						<tr>
							<td>Environnement économique</td>
							<td>30</td>
							<td>1.5</td>
						</tr>
						<tr>
							<td>Fonctionnement des organisations</td>
							<td>42</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Fondamentaux de la communication</td>
							<td>24</td>
							<td>2</td>
						</tr>
						<tr>
							<td>Anglais et informatique</td>
							<td>24</td>
							<td>1.5</td>
						</tr>
					</table>
				</div>
				<div class="container-content" style="display: none;">
					<h3>Semestre 2</h3>
					<table>
						<tr>
							<th>Catégorie</th>
							<th>Discipline</th>
							<th>Volume horaire</th>
							<th>Coefficients</th>
						</tr>
						<tr class="title-UE">
							<td colspan="2">Approfondissements en informatique</td>
							<td>239</td>
							<td>16</td>
						</tr>
						<tr>
							<td rowspan="7"></td>
							<td>Architecture et programmation des mécanismes de base d'un système informatique</td>
							<td>28</td>
							<td>1.5</td>
						</tr>
						<tr>
							<td>Architecture des réseaux</td>
							<td>28</td>
							<td>1.5</td>
						</tr>
						<tr>
							<td>Bases de la programmation orientée objet (POO)</td>
							<td>58</td>
							<td>3.5</td>
						</tr>
						<tr>
							<td>Bases de la conception objet</td>
							<td>42</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Introduction aux IHM</td>
							<td>41</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Programmation et administration des bases de données</td>
							<td>42</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Projet tutoré</td>
							<td>0 (Travail personnel)</td>
							<td>2</td>
						</tr>
						<tr class="title-UE">
							<td colspan="2">Approfondissements en culture, scientifique, sociale et humaine</td>
							<td>224</td>
							<td>24</td>
						</tr>
						<tr>
							<td rowspan="7"></td>
							<td>Graphes et langages</td>
							<td>44</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>Analyse et méthodes numériques</td>
							<td>30</td>
							<td>2</td>
						</tr>
						<tr>
							<td>Environnement comptable, financier, juridique et social</td>
							<td>42</td>
							<td>3</td>
						</tr>
						<tr>
							<td>Gestion de projet informatique</td>
							<td>30</td>
							<td>1.5</td>
						</tr>
						<tr>
							<td>Communication, information et argumentation</td>
							<td>24</td>
							<td>1.5</td>
						</tr>
						<tr>
							<td>Communiquer en anglais</td>
							<td>38</td>
							<td>2.5</td>
						</tr>
						<tr>
							<td>PPP</td>
							<td>24</td>
							<td>1</td>
						</tr>
					</table>
				</div>
				<div class="container-content" style="display: none;">
					<blockquote class="citation-main">
						<p style="margin: initial; font-style: italic;">Qu’est-ce que l’on étudie en general dans cet IUT ?</p>
						<div class="citation-inner">
							<p>Coté informatique on Les langages informatiques, leur structure. La structure des réseaux informatiques, ainsi que des calculs théoriques. Mais on étudie toujours des matières générales comme de l’anglais, de l’expression-communication ou encore de la gestion et du droit qui sont des matières de très importantes.</p>
						</div>
						<div class="citation-author">
							<svg width="30" height="30" viewBox="0 0 70 70" class="citation-svg">
								<polygon points="0,0 10,0 10,10 0,10"/>
								<polygon points="10,10 20,10 20,20 10,20"/>
								<polygon points="20,20 30,20 30,30 20,30"/>
								<polygon points="30,30 40,30 40,40 30,40"/>
								<polygon points="40,40 50,40 50,50 40,50"/>
								<polygon points="50,50 60,50 60,60 50,60"/>
								<polygon points="60,0 70,0 70,70 60,70"/>
							</svg>
							<div class="citation-name">Mot d'étudiant</div>
						</div>
					</blockquote>
				</div>
			</div>
		</div>
		<div class="content-part">
			<div class="title-content">
				<div>
					<h2>Projets</h2>
				</div>
			</div>
			<div class="section-content">
				<div class="menu-content">
					<li>Qu'est ce que c'est ?</li>
					<li>Un exemple</li>
				</div>
				<div class="container-content">
					<p>
						Au cours des deux ans du DUT Informatique différents projets en groupe doivent être réalisés.
						Ils permettent de développer ses compétences en informatiques ainsi que d'autres compétences comme la gestion de projets et le travail en groupe.
					</p>
				</div>
				<div class="container-content" style="display: none;">
					<p>
						Voici le resultat de notre projet personnel d'algorithmique de premier semestre pour vous donner une idée :
					</p>
					<div id="boutonDownload"><a href="include/ProjetProgramme.exe">Télécharger</a></div>
				</div>
			</div>
		</div>
</main>
<footer>
	<?php include 'include/footer-1.php'?>
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="include/chartist.min.js"></script>
<script src="script/script.js"></script>
</body>
</html>