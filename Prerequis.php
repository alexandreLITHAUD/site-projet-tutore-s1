<html>
<head>
	<title>Prérequis</title>
	<?php include 'include/head-1.php'?>
<body>
<header>
	<?php include 'include/header-1.php'?>
	<div class="second-title">
		<div class="second-title-inner">
			<img src="images/Prerequis_titre2.png" />
		</div>
	</div>
</header>

<main class="main-content">
		<div class="content-part">
			<div class="title-content">
				<div>
					<h2>Les prérequis du DUT Informatique</h2>
				</div>
			</div>
			<div class="section-content">
				<div class="menu-content">
					<li>Ce qu'il faut savoir</li>
					<li>Les attendus</li>
				</div>
				<div class="container-content" style="display: none;">
					<p>
						Comme dans toutes études supérieures le DUT Informatique nécessite quelques prérequis, mais il faut garder en tête que lorsque vous arrivez à l'IUT, vous venez pour apprendre. Il <strong>ne faut pas être un pro en programmation</strong> pour accéder à ce DUT.
					</p>
					<blockquote class="citation-main">
						<p style="margin: initial; font-style: italic;">Question : Est-il nécessaire, selon toi, d’avoir des connaissances en informatique avant son arrivée a l'IUT ?</p>
						<div class="citation-inner">
							<p>C’est un plus, mais ce n’est pas nécessaire. Par exemple une ancienne étudiante en droit s’en sort très bien.</p>
						</div>
						<div class="citation-author">
							<svg width="30" height="30" viewBox="0 0 70 70" class="citation-svg">
								<polygon points="0,0 10,0 10,10 0,10"/>
								<polygon points="10,10 20,10 20,20 10,20"/>
								<polygon points="20,20 30,20 30,30 20,30"/>
								<polygon points="30,30 40,30 40,40 30,40"/>
								<polygon points="40,40 50,40 50,50 40,50"/>
								<polygon points="50,50 60,50 60,60 50,60"/>
								<polygon points="60,0 70,0 70,70 60,70"/>
							</svg>
							<div class="citation-name">Mot d'étudiant</div>
						</div>
					</blockquote>
				</div>
				<div class="container-content" style="display: none;">
					<p>
						Bien que les étudiants viennent dans ce DUT pour apprendre, quelques connaissances sont nécessaires.
						<ul>
							<li>Un bon niveau en mathématique (n'ayez pas peur, cela ne déterminera pas votre DUT, vous devrez simplement donner un peu plus d'efforts)</li>
							<li>Une expression française correcte et une bonne maîtrise de l'anglais</li>
							<li>Ouverture d'esprit, rigueur, autonomie</li>
							<li>Logique, esprit d'analyse et de synthèse</li>
						</ul>
					</p>
					<blockquote class="citation-main">
						<p style="margin: initial; font-style: italic;">Selon toi, quelles sont les qualités à avoir pour ce dut ? ?</p>
						<div class="citation-inner">
							<p>Cette formation requière de la patience et de la logique ainsi qu’une grande rigueur surtout dans les travaux pratiques.</p>
						</div>
						<div class="citation-author">
							<svg width="30" height="30" viewBox="0 0 70 70" class="citation-svg">
								<polygon points="0,0 10,0 10,10 0,10"/>
								<polygon points="10,10 20,10 20,20 10,20"/>
								<polygon points="20,20 30,20 30,30 20,30"/>
								<polygon points="30,30 40,30 40,40 30,40"/>
								<polygon points="40,40 50,40 50,50 40,50"/>
								<polygon points="50,50 60,50 60,60 50,60"/>
								<polygon points="60,0 70,0 70,70 60,70"/>
							</svg>
							<div class="citation-name">Mot d'étudiant</div>
						</div>
					</blockquote>
				</div>
			</div>
		</div>
		<div class="content-part">
			<div class="title-content">
				<div>
					<h2>Le baccalauréat</h2>
				</div>
			</div>
			<div class="section-content">
				<div class="menu-content">
					<li>Que faut-il avoir ?</li>
				</div>
				<div class="container-content">
					<p>
						Pour accéder au DUT Informatique il faut être titulaire d'un baccalauréat. Les bacheliers les plus recherchés en informatique viennent de S ou STI2D, mais d'autres BAC sont évidemment acceptés. L'admission se faisant sur dossier, un bon parcours au lycée aide à être accepté plus facilement.
					</p>
					<div id="GraphCirculaireBac">
						<div>
							<span>Autres <strong>2%</strong></span>
							<span>Bac professionnel <strong>3%</strong></span>
							<span>Bac technologique <strong>30%</strong></span>
							<span>Bac général <strong>65%</strong></span>
						</div>
						<img src="images/GraphCirculaireBac.png" />
					</div>
				</div>
			</div>
		</div>
		<div class="content-part">
			<div class="title-content">
				<div>
					<h2>L'inscription</h2>
				</div>
			</div>
			<div class="section-content">
				<div class="menu-content">
					<li>Parcoursup</li>
					<li>L'inscription au DUT Informatique de Dijon</li>
				</div>
				<div class="container-content" style="display: none;">
						<p>Pour accéder au DUT Informatique vous devrez premièrement accéder à Parcoursup. Si vous êtes en établissement scolaire vos professeurs vous accompagneront dans la démarche d'inscription qui ouvre le 20 décembre.</p>
					
						<p>Le principe de Parcoursup est de formuler des voeux dans différents établissements qui analyseront par la suite votre dossier. Suite à votre inscription différentes démarches sont à effectuer..</p>
						<div class="card-menu">
							<div class="card-content card-phase-1" onclick="buttonAppearText(1)">
								<span class="card-title">22 janvier - 12 mars : Formulation des voeux</span>
							</div>
							<div class="card-content card-phase-2" onclick="buttonAppearText(2)">
								<span class="card-title">2 avril : Limite de la phase d'admission</span>
							</div>
							<div class="card-content card-phase-3" onclick="buttonAppearText(3)">
								<span class="card-title">19 mai - 17 juillet</span>
							</div>
							<div class="card-content card-phase-4" onclick="buttonAppearText(4)">
								<span class="card-title">Début juillet</span>
							</div>
							<div class="card-content card-phase-5" onclick="buttonAppearText(5)">
								<span class="card-title">Fin juin - 11 septembre</span>
							</div>
						</div>

						<div class="card-text text-phase-1">
							<p>Cette période paraît longue mais vous devez impérativement réflechir aux différents voeux que vous allez faire, vous en avez une limite de 10. Vous n'êtes pas seuls à postuler, plus vous serez rapide et sûr de vous, mieux se déroulera vos démarches.</p>
						</div>
						<div class="card-text text-phase-2">
							<p>Il s'agît de la date butoire pour finaliser votre dossier. Il est obligatoire d'avoir rempli tous les champs nécessaires, si il manque des informations les établissements ne pourront pas traiter correctement votre dossier.</p>
						</div>
						<div class="card-text text-phase-3">
							<p>Il s'agît du moment décisif, vous reçevrez au fur et à mesure les différentes réponses. Il y a alors différentes possibilités :
							<ul>
								<li>Vous êtes accepté : l'établissement vous offre une proposition d'admission, vous pouvez alors accepté ou refusé. Il est préférable de répondre assez rapidement.</li>
								<li>Vous êtes en file d'attente : vous devez attendre que les places se libère, si des personnes refusent leur proposition vous avancerez dans la file. Il faut savoir être patient lors de cette phase.</li>
								<li>Vous êtes refusé : vous n'avez pas de choix ici, vous n'aurez pas accès à cette formation.</li>
							</ul>
							</p>
							<p>Ce délai ,très long, possède en réalité plusieurs phases, la procédure est suspendu lors des épreuves du baccalauréat et reprend le 29 juin jusqu'au 17 juillet. Surtout n'oubliez pas de refusé les propositions que vous ne désirez pas! Des étudiants attendent...</p>
						</div>
						<div class="card-text text-phase-4">
							<p>Les inscriptions débutent dans les établissements, si vous avez déjà été accepté dans un d'eux vous pouvez commencez à vous inscrire.</p>
						</div>
						<div class="card-text text-phase-5">
							<p>Vous pouvez formuler 10 nouveaux voeux complémentaires dans les établissements qui ont encore des places de disponibles.</p>
							<br />
							<p>Pour plus d'informations, rendez-vous sur le site de Parcoursup en cliquant <a href='https://www.parcoursup.fr/'>ici</a>.</p>
						</div>
				</div>
				<div class="container-content" style="display: none;">
						<p>L'établissement examine votre dossier, si un équilibre en les différentes matières est présente, si votre comportement correspond à celui attendu,... Une fois que vous avez été accepté sur Parcoursup et que vous avez confirmé votre venu dans la formation, l'inscription débute en juillet et se déroule entièrement en ligne. Des dossiers seront mis à votre disposition. Des informations complémentaires ainsi que des papiers vous seront demandés. Pour vous inscrire vous aurez obligatoirement besoin de votre <a href='https://cvec.etudiant.gouv.fr/'>CVEC</a>, pour un coût de 90€. Les frais d'inscriptions s'élèvent ensuite à 170€.</p>
				</div>
			</div>
		</div>
</main>
<footer>
	<?php include 'include/footer-1.php'?>
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="include/chartist.min.js"></script>
<script src="script/script.js"></script>
</body>
</html>